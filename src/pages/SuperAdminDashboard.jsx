import './SuperAdminDashboard.css'
export default function SuperAdminDashboard() {
  return (
    <div className="super-admin-dashboard">
      <div className="container-50">
        <div className="normal-side-bar">
          <div className="dashboard">
          <img class="vector-79" src="../assets/vectors/vector_18_x2.svg" />
            <span className="text-1">
            Dashboard
            </span>
          </div>
          <div className="copyright-2">
          Main
          </div>
          <div className="line-115">
          </div>
          <div className="administration">
          <img class="vector-78" src="../assets/vectors/vector_233_x2.svg" />
            <div className="admin-payrol">
            Admin &amp; Payrol
            </div>
          </div>
          <div className="transport">
            <div className="icon-park-outlinetransporter-1">
            <img class="group-2" src="../assets/vectors/group_3_x2.svg" />
            </div>
            <span className="operations-transport">
            Operations &amp;<br />
             Transport
            </span>
          </div>
          <div className="marketing-4">
          <img class="vector-83" src="../assets/vectors/vector_214_x2.svg" />
            <span className="legal">
            Legal
            </span>
          </div>
          <div className="finance-1">
            <div className="material-symbols-lightfinance-mode">
            <img class="vector-75" src="../assets/vectors/vector_170_x2.svg" />
            </div>
            <span className="finance-accounting">
            Finance &amp; <br />
            Accounting
            </span>
          </div>
          <div className="partnership">
            <span className="business-develop-ment-partnerships">
            Business develop-<br />
            ment &amp; Partnerships
            </span>
            <div className="mdipartnership-outline">
            <img class="vector-74" src="../assets/vectors/vector_43_x2.svg" />
            </div>
          </div>
          <div className="marketing">
            <div className="nimbusmarketing">
            <img class="vector-82" src="../assets/vectors/vector_10_x2.svg" />
            </div>
            <span className="marketing-communications">
            Marketing &amp;<br />
            Communications
            </span>
          {/* </div>
            <div className="marketing-1">
              <div className="nimbusmarketing-1">
              <img class="vector-84" src="../assets/vectors/vector_90_x2.svg" />
              </div> */}
            <span className="customer-experience">
            Customer <br />
            Experience<br />
            
            </span>
          </div>
          <div className="marketing-2">
            <div className="nimbusmarketing-2">
            <img class="vector-85" src="../assets/vectors/vector_235_x2.svg" />
            </div>
            <span className="messaging-meetings">
            Messaging &amp; <br />
            Meetings<br />
            
            </span>
          </div>
          <div className="marketing-3">
            <div className="nimbusmarketing-3">
            <img class="ouiintegration-general" src="../assets/vectors/ouiintegration_general_2_x2.svg" />
            </div>
            <span className="apps-integration">
            Apps &amp; Integration
            </span>
          </div>
          <div className="copyright">
          Settings
          </div>
          <div className="line-114">
          </div>
          <div className="marketing-5">
            <div className="nimbusmarketing-4">
            <img class="vector-80" src="../assets/vectors/vector_1_x2.svg" />
            </div>
            <span className="help">
            Help
            </span>
          </div>
          <div className="marketing-6">
            <span className="settings">
            Settings
            </span>
          </div>
          <div className="marketing-7">
          <img class="vector-81" src="../assets/vectors/vector_44_x2.svg" />
            <span className="log-out">
            Log Out
            </span>
          </div>
          <span className="copyright-1">
          © 2024 LIMS. All Rights Reserved
          </span>
        </div>
        {/* <div className="group-1000002790">
          <div className="rectangle-1406">
          </div>
        </div> */}
      </div>
      <div className="contrainer-58">
        <div className="top-bar">
          <div className="container-5">
            <div className="container-22">
              <div className="search-bar-1">
                <div className="magnifier">
                <img class="xmlid-223" src="../assets/vectors/xmlid_2235_x2.svg" />
                </div>
                <span className="search-anythingfiles-documents-task">
                Search anything,files, documents, task 
                </span>
              </div>
              <div className="frame-92">
                <div className="container-48">
                  <div className="container-42">
                    <div className="mingcutetime-line">
                    <img class="vector-112" src="../assets/vectors/vector_226_x2.svg" />
                    </div>
                    <div className="container-34">
                    2 : 35
                    </div>
                  </div>
                  <div className="news-notification-component">
                    <div className="notification">
                    {/* <img class="claritynotification-line-1" src="../assets/vectors/claritynotification_line_6_x2.svg" />
                    <img class="vector-95" src="../assets/vectors/vector_144_x2.svg" /> */}
                      <span className="container-32">
                      3
                      </span>
                    </div>
                    <div className="news-cover">
                    <img class="polygon-3" src="../assets/vectors/polygon_31_x2.svg" />
                      <div className="frame-109">
                        <span className="news">
                        News
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <span className="thurs-1-jan">
                Thurs, 1 Jan
                </span>
              
              </div>
              <div class="quick-actions-components">
                <img class="additional-menu" src="../assets/vectors/additional_menu_8_x2.svg" />
              </div>
            </div>

            <div className="languange">
              <div className="text-2">
              <img class="usa-1" src="../assets/vectors/usa_x2.svg" />
                <span className="eng">
                Eng
                </span>
              </div>
              <div className="chevron-down">
              <img class="vector-111" src="../assets/vectors/vector_161_x2.svg" />
              </div>
            </div>
          </div>
          {/* <div class="chevron-down">
                <img class="vector-111" src="../assets/vectors/vector_161_x2.svg" />
              </div> */}
            
          <div className="menu">
            <div className="container-45">
              <div className="quick-actions-components">
              <img class="container-20" src="../assets/vectors/container_12_x2.svg" />
              </div> 
               <div className="notification-component">
                <div className="container-44">
                <img class="container-20" src="../assets/vectors/container_12_x2.svg" />
                </div>
              </div> 
            </div>
            <div className="container-1">
              <div className="mask-group">
                <div className="container-33">
                </div>
              </div>
              <div className="user-profile">
                <div className="container-9">
                  <span className="nromaric">
                  N. Romaric
                  </span>
                  <div className="group-21861">
                  <img class="container-20" src="../assets/vectors/container_12_x2.svg" />
                  </div>
                </div>
                <span className="sadmin-dg">
                S Admin: DG
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="component-1">
          <span className="add">
          + Add
          </span>
          <div className="frame-941">
            <span className="add">
            + Add
            </span>
          </div>
          <div className="frame-931">
            <div className="frame-941">
              <span className="add">
              + Add
              </span>
            </div>
          </div>
          <div className="hi-romaric">
          Hi Romaric!
          </div>
          <span className="your-working-summary">
          Your working summary
          </span>
        </div>
        <div className="container-29">
          <div className="frame-93">
            <div className="container-27">
              <div className="operations-transport-1">
              Operations &amp; transport
              </div>
              <div className="rimore-fill-8">
              <img class="vector-117" src="../assets/vectors/vector_111_x2.svg" />
              </div>
            </div>
            <div className="small-description">
            Shipment , Inventory and Warehouse tracking
            </div>
            <div className="container-53">
              <div className="frame-96">
                <div className="frame-99">
                  <div className="icon-3">
                  <img class="container-43" src="../assets/vectors/container_100_x2.svg" />
                  </div>
                  <div className="container-35">
                  546
                  </div>
                  <div className="shipment-6">
                  Shipment
                  </div>
                  <div className="group-1000002788">
                  <img class="shape-1" src="../assets/vectors/shape_4_x2.svg" />
                    <span className="day">
                    +4% (1 day)
                    </span>
                  </div>
                </div>
              </div>
              <div className="frame-97">
                <div className="icon-4">
                <img class="vector-115" src="../assets/vectors/vector_199_x2.svg" />
                </div>
                <div className="k">
                300K
                </div>
                <div className="inventory">
                Inventory
                </div>
                <div className="group-1000002786">
                <img class="shape-2" src="../assets/vectors/shape_12_x2.svg" />
                  <span className="days">
                  -4% (2 days)
                  </span>
                </div>
              </div>
              <div className="frame-98">
                <div className="icon-5">
                <img class="vector-116" src="../assets/vectors/vector_189_x2.svg" />
                </div>
                <div className="container-36">
                896
                </div>
                <div className="warehouse">
                Warehouse
                </div>
                <div className="group-1000002785">
                <img class="shape-3" src="../assets/vectors/shape_7_x2.svg" />
                  <span className="days-1">
                  +4% (6 days)
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="frame-94">
            <div className="actions">
            Actions
            </div>
            <div className="frame-117">
              <div className="user">
              <img class="user-1" src="../assets/vectors/user_x2.svg" />
              </div>
              <span className="customize-dashboard">
              Customize Dashboard
              </span>
            </div>
            <div className="frame-119">
              <div className="streamlinegraph-bar-decrease">
              <img class="vector-8" src="../assets/vectors/vector_132_x2.svg" />
              </div>
              <span className="view-cashflow-report">
              View Cashflow Report
              </span>
            </div>
            <div className="frame-120">
              <div className="icon-park-outlinetransporter">
              <img class="group" src="../assets/vectors/group_1_x2.svg" />
              </div>
              <span className="schedule-shipment">
              Schedule Shipment
              </span>
            </div>
          </div>
           <div className="frame-94">
            <div className="container-3">
              <div className="finance">
              Finance
              </div>
              <img class="vector-17" src="../assets/vectors/vector_37_x2.svg" />
            </div>
            <div className="finance-tracking-activities">
            Finance tracking activities
            </div>
            <div className="container-4">
              <div className="small-balance">
                <div className="container-35">
                  <div className="icon-1">
                    <div className="icon-2">
                    <img class="vector-15" src="../assets/vectors/vector_149_x2.svg" />
                    </div>
                  </div>
                  <div className="label-9">
                  Balance
                  </div>
                </div>
                <span className="cash-4">
                10k M XAF
                </span>
                <div className="precentage">
                  <span className="container-25">
                  +23%
                  </span>
                  <span className="since-last-month-2">
                  since last month
                  </span>
                </div>
              </div>
              <div className="small-sales">
                <div className="container-58">
                  <div className="container-10">
                    <div className="streamlineinvestment-selection">
                    <img class="group-1" src="../assets/vectors/group_8_x2.svg" />
                    </div>
                  </div>
                  {/* <div className="text"> */}
                    <span className="label-9">
                    Investment
                    </span>
                    <span className="cash-4">
                    57k XAF
                    </span>
                  </div>
                {/* </div> */}
                <span className="container-25">
                +23%
                </span>
                <span className="since-last-month-1">
                since last month
                </span>
              </div>
            </div>
            <div className="container-35">
              <div className="small-earnings-1">
                <div className="container-35">
                  <div className="icone-1">
                  <img class="vector-12" src="../assets/vectors/vector_241_x2.svg" />
                  </div>
                  <span className="label-9">
                  Investment
                  </span>
                </div>
                <span className="cash-4">
                57k XAF
                </span>
                <div className="precentage">
                  <span className="container-25">
                  +23%
                  </span>
                  <span className="since-last-month-3">
                  since last month
                  </span>
                </div>
              </div>
              <div className="small-earnings">
                <div className="container-8">
                  <div className="icon">
                    <div className="streamlinegraph-bar-decrease-1">
                    <img class="vector-12" src="../assets/vectors/vector_151_x2.svg" />
                    </div>
                  </div>
                  <div className="label-9">
                  Spend this month
                  </div>
                </div>
                <span className="cash-4">
                3M XAF
                </span>
                <span className="container-25">
                +23%
                </span>
                <span className="since-last-month">
                since last month
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="container-22">
          <div className="frame-104">
            <div className="header-3">
              <div className="container-38">
                <div className="revenue">
                Revenue
                </div>
                <div className="timeline-button">
                  <span className="total">
                  Total 
                  </span>
                  <div className="arrow-drop-up">
                  <img class="vector-9" src="../assets/vectors/vector_172_x2.svg" />
                  </div>
                </div>
              </div>
              <div className="rimore-fill-3">
              <img class="vector-3" src="../assets/vectors/vector_218_x2.svg" />
              </div>
            </div>
            <div className="xaf-57680-k">
            XAF 576.80K
            </div>
            <img class="chart" src="../assets/vectors/chart_x2.svg" />
            <div className="content">
              <div className="content-1">
                <div className="sent">
                  <div className="container-46">
                    <div className="ellipse-61">
                    </div>
                    <span className="charges">
                    Charges
                    </span>
                  </div>
                  <span className="container">
                  60%
                  </span>
                </div>
                <div className="container-49">
                  <div className="separator">
                  </div>
                  <div className="recieved-1">
                    <div className="container-5">
                      <div className="ellipse-612">
                      </div>
                      <span className="adds">
                      Adds
                      </span>
                    </div>
                    <span className="container-2">
                    23%
                    </span>
                  </div>
                </div>
                <div className="recieved">
                  <div className="container-35">
                    <div className="ellipse-611">
                    </div>
                    <span className="billing">
                    Billing
                    </span>
                  </div>
                  <span className="container-1">
                  17%
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="frame-103">
            <div className="header">
              <div className="container-57">
                <div className="recent-transactions">
                Recent Transactions
                </div>
                <div className="rimore-fill">
                <img class="vector" src="../assets/vectors/vector_169_x2.svg" />
                </div>
              </div>
              <span className="most-recent-transactions">
              Most recent transactions
              </span>
            </div>
            <div className="transaction">
              <div className="container-23">
                {/* <div className="recieve">
                  <div className="money-recive">
                  <img class="money-recive-4" src="../assets/vectors/money_recive_x2.svg" />
                  </div>
                </div> */}
                {/* <div className="frame-289">
                  <div className="shipment-payment">
                  Shipment Payment
                  </div>
                  <span className="lilly-james">
                  Lilly James
                  </span>
                </div> */}
              </div>
              {/* <div className="container">
                <div className="xaf-80000">
                + XAF 80,000
                </div>
                <span className="hours-ago">
                2hours ago
                </span>
              </div> */}
            </div>
            <div className="transaction-1">
              <div className="container-12">
                <div className="recieve-1">
                  <div className="money-recive-1">
                  <img class="money-recive-5" src="../assets/vectors/money_recive_3_x2.svg" />
                  </div>
                </div>
                <div className="frame-2891">
                  <div className="invoice-payment">
                  Invoice Payment
                  </div>
                  <span className="lauret-micheal">
                  Lauret Micheal
                  </span>
                </div>
              </div>
              <div className="container-61">
                <div className="xaf-5000">
                + XAF 5,000
                </div>
                <span className="hours-ago-1">
                23hours ago
                </span>
              </div>
            </div>
            <div className="transaction-2">
              <div className="container-12">
                <div className="recieve-2">
                  <div className="money-recive-2">
                  <img class="money-recive-6" src="../assets/vectors/money_recive_1_x2.svg" />
                  </div>
                </div>
                <div className="frame-2892">
                  <div className="invoice-payment-1">
                  Invoice Payment
                  </div>
                  <span className="frank-jordan">
                  Frank Jordan
                  </span>
                </div>
              </div>
              <div className="container-36">
                <div className="xaf-50001">
                + XAF 5,000
                </div>
                <span className="day-ago">
                1day ago
                </span>
              </div>
            </div>
            <div className="transaction-3">
              <div className="container-12">
                <div className="recieve-3">
                  <div className="money-recive-3">
                  <img class="money-recive-7" src="../assets/vectors/money_recive_4_x2.svg" />
                  </div>
                </div>
                <div className="frame-2893">
                  <div className="shipment-payment-1">
                  Shipment Payment
                  </div>
                  <span className="lilly-james-1">
                  Lilly James
                  </span>
                </div>
              </div>
              <div className="container-36">
                <p className="xaf-800001">+ XAF 8,000
                <span className="xaf-800001-sub-0"></span><span className="xaf-800001-sub-1">  </span><span></span>
                </p>
                <span className="day-ago-1">
                1day ago
                </span>
              </div>
            </div>
          </div>
          <div className="frame-102">
  <div className="header-1">
    <div className="container-39">
      <div className="invoices">Invoices</div>
      <div className="rimore-fill-1">
        <img className="vector-1" src="../assets/vectors/vector_216_x2.svg" />
      </div>
    </div>
    <span className="recently-created-invoices">Recently created invoices</span>
  </div>
  <div className="invoice-summary-table">
    <div className="table-head">
      <span className="name">Name</span>
      <span className="date">Date</span>
      <span className="status">Status</span>
    </div>
    <div className="table-content">
      <div className="row">
        <div className="invoice-1">Invoice 1</div>
        <div className="container-5">12/03/2024</div>
        <div className="status-1">
          <span className="unpaid">Unpaid</span>
        </div>
      </div>
      <div className="row-1">
        <div className="invoice-2">Invoice 2</div>
        <div className="container-5">13/03/2024</div>
        <div className="status-2">
          <span className="paid-1">Paid</span>
        </div>
      </div>
      <div className="row-2">
        <div className="invoice-3">Invoice 3</div>
        <div className="container-5">16/03/2024</div>
        <div className="status-3">
          <span className="paid-1">Paid</span>
        </div>
      </div>
      <div className="row-3">
        <div className="invoice-4">Invoice 4</div>
        <div className="container-6">18/03/2024</div>
        <div className="status-4">
          <span className="unpaid-1">Unpaid</span>
        </div>
      </div>
    </div>
  </div>
</div>
        </div>
        <div className="container-60">
          <div className="container-13">
            <div className="frame-107">
              <div className="header-2">
                <div className="container-26">
                  <div className="top-customers">
                  Top Customers
                  </div>
                  <div className="rimore-fill-2">
                  <img class="vector-2" src="../assets/vectors/vector_202_x2.svg" />
                  </div>
                </div>
                <span className="view-customers-with-higher-purchase">
                View customers with higher purchase
                </span>
              </div>
              <div className="total-customers">
                <span className="cash">
                2570
                </span>
                <div className="label">
                Total Customers
                </div>
              </div>
              <div className="top-customer-table">
                <div className="table-head-1">
                  <span className="customer">
                  Customer
                  </span>
                  <span className="total-purchase">
                  Total Purchase
                  </span>
                </div>
                <div className="customers">
                  <div className="row-4">
                    <div className="customer-1">
                      <div className="image-290">
                      </div>
                      <div className="customer-text">
                        <div className="frank-jordan-1">
                        Frank Jordan
                        </div>
                        <span className="frankjordan-7-gmail-com">
                        frankjordan7@gmail.com
                        </span>
                      </div>
                    </div>
                    <div className="xaf-950-k">
                    XAF 950K
                    </div>
                  </div>
                  <div className="row-5">
                    <div className="customer-2">
                      <div className="image-288">
                      </div>
                      <div className="customer-text-1">
                        <div className="lilian-bache">
                        Lilian Bache
                        </div>
                        <span className="lilianbache-16-gmail-com">
                        lilianbache16@gmail.com
                        </span>
                      </div>
                    </div>
                    <div className="xaf-820-k">
                    XAF 820K
                    </div>
                  </div>
                  <div className="row-6">
                    <div className="customer-3">
                      <div className="frame-290">
                        <span className="n">
                        N
                        </span>
                      </div>
                      <div className="customer-text-2">
                        <div className="nelson-ngoe">
                        Nelson Ngoe
                        </div>
                        <span className="nelsonngoe-1-gmail-com">
                        nelsonngoe1@gmail.com
                        </span>
                      </div>
                    </div>
                    <div className="xaf-700-k">
                    XAF 700K
                    </div>
                  </div>
                </div>
              </div>
              <div className="row-20">
                <div className="customer-4">
                  <div className="image-2881">
                  </div>
                  <div className="customer-text-3">
                    <div className="lilian-bache-1">
                    Lilian Bache
                    </div>
                    <span className="lilianbache-16-gmail-com-1">
                    lilianbache16@gmail.com
                    </span>
                  </div>
                </div>
                <div className="xaf-820-k-1">
                XAF 820K
                </div>
              </div>
            </div>
            <div className="frame-108">
              <div className="header-6">
                <div className="container-50">
                  <div className="messages">
                  Messages
                  </div>
                  <div className="rimore-fill-7">
                  <img class="vector-11" src="../assets/vectors/vector_207_x2.svg" />
                  </div>
                </div>
                <span className="recent-messages">
                Recent Messages
                </span>
              </div>
              <div className="messages-1">
                <div className="frame-76110">
                  <div className="frame-293">
                    <div className="image-287">
                    </div>
                    <div className="frame-2894">
                      <div className="lilly-james-2">
                      Lilly James
                      </div>
                      <span className="hello-ijust-wanted-to-find-out-more-about">
                      Hello, I just wanted to find out more about...
                      </span>
                    </div>
                  </div>
                  <span className="m">
                  4m
                  </span>
                </div>
                <div className="frame-76111">
                  <div className="frame-2932">
                    <div className="image-2901">
                    </div>
                    <div className="frame-2896">
                      <div className="frank-jordan-2">
                      Frank Jordan
                      </div>
                      <span className="hello-ijust-wanted-to-find-out-more-about-2">
                      Hello, I just wanted to find out more about...
                      </span>
                    </div>
                  </div>
                  <span className="m-1">
                  14m
                  </span>
                </div>
                <div className="frame-76112">
                  <div className="frame-2933">
                    <div className="image-2882">
                    </div>
                    <div className="frame-2897">
                      <div className="lilian-bache-2">
                      Lilian Bache
                      </div>
                      <span className="hello-ijust-wanted-to-find-out-more-about-3">
                      Hello, I just wanted to find out more about...
                      </span>
                    </div>
                  </div>
                  <span className="m-2">
                  30m
                  </span>
                </div>
                <div className="frame-76113">
                  <div className="frame-2931">
                    <div className="image-2871">
                    </div>
                    <div className="frame-2895">
                      <div className="lilly-james-3">
                      Lilly James
                      </div>
                      <span className="hello-ijust-wanted-to-find-out-more-about-1">
                      Hello, I just wanted to find out more about...
                      </span>
                    </div>
                  </div>
                  <span className="hrs">
                  4hrs
                  </span>
                </div>
                <div className="frame-76114">
                  <div className="frame-2934">
                    <div className="image-2902">
                    </div>
                    <div className="frame-2898">
                      <div className="frank-jordan-3">
                      Frank Jordan
                      </div>
                      <span className="hello-ijust-wanted-to-find-out-more-about-4">
                      Hello, I just wanted to find out more about...
                      </span>
                    </div>
                  </div>
                  <span className="m-3">
                  14m
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="shipments-in-transit">
            <div className="top">
              <div className="container-59">
                <div className="freight-management">
                Freight Management
                </div>
                <div className="rimore-fill-6">
                <img class="vector-10" src="../assets/vectors/vector_52_x2.svg" />
                </div>
              </div>
              <span className="view-shipments-currently-in-transit">
              View shipments currently In transit
              </span>
            </div>
            <div className="map-group">
              <div className="map">
                <div className="ihg-59-t-4-cw-4-ox-gct-1-ksr-qzw-1">
                </div>
                <div className="container-47">
                  <div className="container-52">
                    <div className="container-34">
                      <div className="position-8">
                      <img class="game-iconsposition-marker-12" src="../assets/vectors/game_iconsposition_marker_3_x2.svg" />
                      </div>
                      <div className="container-16">
                        <div className="container-27">
                        +
                        </div>
                        <div className="vector-11">
                        </div>
                        <div className="container-28">
                        -
                        </div>
                      </div>
                    </div>
                    <div className="container-19">
                      <div className="container-30">
                      <img class="game-iconsposition-marker-11" src="../assets/vectors/game_iconsposition_marker_17_x2.svg" />
                       </div>
                       <div>
                       <img class="container-55" src="../assets/vectors/container_48_x2.svg" />
                        <div className="container-56">
                          <div className="container-54">
                            <div className="group-1000002789">
                              <div className="rectangle-1404">
                              </div>
                              <img class="polygon-2" src="../assets/vectors/polygon_2_x2.svg" />
                              <span className="country">
                              country
                              </span>
                              <span className="l-45-l-98">
                              L: 45 l:98
                              </span>
                            </div>
                            <div className="position-3">
                            <img class="game-iconsposition-marker-4" src="../assets/vectors/game_iconsposition_marker_10_x2.svg" />
                            </div>
                            <img class="vector-7" src="../assets/vectors/vector_71_x2.svg" />
                          </div>
                          <div className="position-1">
                          <img class="game-iconsposition-marker-2" src="../assets/vectors/game_iconsposition_marker_19_x2.svg" />
                          </div>
                        </div>
                      </div>
                      <div className="position-9">
                      <img class="game-iconsposition-marker-14" src="../assets/vectors/game_iconsposition_marker_23_x2.svg" />
                      </div>
                      <div className="position-4">
                      <img class="game-iconsposition-marker-5" src="../assets/vectors/game_iconsposition_marker_28_x2.svg" />
                      </div>
                      <img class="game-iconsposition-marker-13" src="../assets/vectors/game_iconsposition_marker_22_x2.svg" />
                      <img class="game-iconsposition-marker-7" src="../assets/vectors/game_iconsposition_marker_5_x2.svg" />
                      <img class="game-iconsposition-marker-8" src="../assets/vectors/game_iconsposition_marker_16_x2.svg" />
                      <div className="position-6">
                      <img class="game-iconsposition-marker-9" src="../assets/vectors/game_iconsposition_marker_26_x2.svg" />
                      </div>
                    </div>
                  </div>
                  <div className="position">
                  <img class="game-iconsposition-marker" src="../assets/vectors/game_iconsposition_marker_15_x2.svg" />
                  </div>
                  <img class="game-iconsposition-marker-1" src="../assets/vectors/game_iconsposition_marker_20_x2.svg" />
                  <div className="position-2">
                  <img class="game-iconsposition-marker-3" src="../assets/vectors/game_iconsposition_marker_6_x2.svg" />
                  </div>
                  <div className="position-7">
                  <img class="game-iconsposition-marker-10" src="../assets/vectors/game_iconsposition_marker_4_x2.svg" />
                  </div>
                  <div className="position-5">
                  <img class="game-iconsposition-marker-6" src="../assets/vectors/game_iconsposition_marker_27_x2.svg" />
                  </div>
                </div>
              </div>
            </div>
            <div className="search-bar">
              <div className="search-normal">
              <img class="search-normal-1" src="../assets/vectors/search_normal_2_x2.svg" />
              </div>
              <div className="search">
              Search...
              </div>
            </div>
            <div className="shipment-table">
              <div className="table-head-4">
                <div className="title-4">
                  <span className="label-3">
                  Name
                  </span>
                  <div className="keyboard-arrow-down-1">
                  <img class="vector-70" src="../assets/vectors/vector_249_x2.svg" />
                  </div>
                </div>
                <div className="title">
                  <span className="label-2">
                  Destination
                  </span>
                  <div className="keyboard-arrow-down">
                  <img class="vector-69" src="../assets/vectors/vector_156_x2.svg" />
                  </div>
                </div>
                <div className="title-1">
                  <span className="label-4">
                  Tracking Number
                  </span>
                  <div className="keyboard-arrow-down-2">
                  <img class="vector-71" src="../assets/vectors/vector_182_x2.svg" />
                  </div>
                </div>
                <div className="title-2">
                  <span className="label-6">
                  Quantity
                  </span>
                  <div className="keyboard-arrow-down-4">
                  <img class="vector-73" src="../assets/vectors/vector_165_x2.svg" />
                  </div>
                </div>
                <div className="title-3">
                  <span className="label-5">
                  Date
                  </span>
                  <div className="keyboard-arrow-down-3">
                  <img class="vector-72" src="../assets/vectors/vector_237_x2.svg" />
                  </div>
                </div>
              </div>
              <div className="container-12">
           <div className="table">
  <div className="row-13">
    <span className="shipment">Shipment 1</span>
    <span className="cameroon">Cameroon</span>
    <span className="container-18">090576</span>
    <span className="kg">500kg</span>
    <span className="mar-2024">19 Mar, 2024</span>
  </div>
  <div className="row-14">
    <span className="shipment">Shipment 2</span>
    <span className="nigeria">Nigeria</span>
    <span className="container-18">190576</span>
    <span className="kg-1">300kg</span>
    <span className="mar-20241">19 Mar, 2024</span>
  </div>
  <div className="row-15">
    <span className="shipment">Shipment 3</span>
    <span className="ghana">Ghana</span>
    <span className="container-18">190576</span>
    <span className="kg-2">700kg</span>
    <span className="mar-20242">19 Mar, 2024</span>
  </div>
  <div className="row-15">
    <span className="shipment">Shipment 4</span>
    <span className="south-korea">South Korea</span>
    <span className="container-22">190576</span>
    <span className="kg-3">700kg</span>
    <span className="mar-20243">19 Mar, 2024</span>
  </div>
  <div className="row-17">
    <span className="shipment">Shipment 6</span>
    <span className="dubai">Dubai</span>
    <span className="container-18">190576</span>
    <span className="kg-4">700kg</span>
    <span className="mar-20244">19 Mar, 2024</span>
  </div>
  <div className="row-18">
    <span className="shipment">Shipment 7</span>
    <span className="germany">Germany</span>
    <span className="container-22">190576</span>
    <span className="kg-5">700kg</span>
    <span className="mar-20245">19 Mar, 2024</span>
  </div>
  <div className="row-19">
    <span className="shipment">Shipment 8</span>
    <span className="usa">USA</span>
    <span className="container-18">190576</span>
    <span className="kg-6">700kg</span>
    <span className="mar-20246">19 Mar, 2024</span>
  </div>
</div>
                <div className="scroll-1">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-32">
          <div className="top-shipments">
            <div className="header-4">
              <div className="container-17">
                <div className="top-shipments-1">
                Top Shipments
                </div>
                <div className="rimore-fill-4">
                <img class="vector-4" src="../assets/vectors/vector_88_x2.svg" />
                </div>
              </div>
              <span className="view-top-shipments">
              View top shipments
              </span>
            </div>
            <div className="total-customers-1">
              <span className="cash-1">
              1203
              </span>
              <div className="label-1">
              Total Shipments
              </div>
            </div>
            <div className="frame-76120">
              <div className="table-head-3">
                <span className="shipment-name-type-1">
                Shipment Name &amp; Type
                </span>
                <span className="price-sold">
                Price Sold
                </span>
                <span className="shipment-progress">
                Shipment Progress
                </span>
              </div>
              <div className="row-10">
                <div className="shipment-3">
                  <div className="image-2913">
                  </div>
                  <div className="texts-9">
                    <div className="shipment-no-11">
                    Shipment No 1
                    </div>
                    <span className="air-freight-3">
                    Air Freight
                    </span>
                  </div>
                </div>
                <div className="xaf-450-k">
                XAF 450K
                </div>
                <div className="revenue-1">
                  <div className="frame-291">
                    <div className="frame-292">
                    </div>
                  </div>
                  <span className="container-13">
                  50%
                  </span>
                </div>
              </div>
              <div className="row-11">
                <div className="shipment-4">
                  <div className="image-2914">
                  </div>
                  <div className="texts-10">
                    <div className="shipment-no-21">
                    Shipment No 2
                    </div>
                    <span className="ocean-freight">
                    Ocean Freight
                    </span>
                  </div>
                </div>
                <div className="xaf-400-k">
                XAF 400K
                </div>
                <div className="revenue-2">
                  <div className="frame-2911">
                    <div className="frame-2921">
                    </div>
                  </div>
                  <span className="container-14">
                  42%
                  </span>
                </div>
              </div>
              <div className="row-12">
                <div className="shipment-5">
                  <div className="image-2915">
                  </div>
                  <div className="texts-11">
                    <div className="shipment-no-31">
                    Shipment No 3
                    </div>
                    <span className="ocean-freight-1">
                    Ocean Freight
                    </span>
                  </div>
                </div>
                <div className="xaf-380-k">
                XAF 380K
                </div>
                <div className="revenue-3">
                  <div className="frame-2912">
                    <div className="frame-2922">
                    </div>
                  </div>
                  <span className="container-15">
                  30%
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="frame-114">
            <div className="header-5">
              <div className="overdue-shipments">
              Overdue Shipments
              </div>
              <div className="rimore-fill-5">
              <img class="vector-5" src="../assets/vectors/vector_23_x2.svg" />
              </div>
            </div>
            <div className="table-head-2">
              <span className="shipment-name-type">
              Shipment Name &amp; Type
              </span>
              <span className="expected-delivery">
              Expected Delivery
              </span>
              <span className="last-scan-location">
              Last Scan Location
              </span>
            </div>
            <div className="container-25">
  <div className="shipments">
    {/* Expédition 1 */}
    <div className="row-7 shipment-row">
      <div className="shipment-1">
        <div className="image-2911"></div>
        <div className="texts-6">
          <div className="shipment-no">Shipment No 1</div>
          <span className="air-freight">Air Freight</span>
        </div>
      </div>
      <div className="texts-4">
        <div className="location">Cameroon, Douala</div>
        <span className="date">12/03/2024</span>
      </div>
      <div className="texts-2">
        <div className="location">Nigeria, Abuja</div>
        <span className="date">18/03/2024</span>
      </div>
    </div>

    {/* Expédition 2 */}
    <div className="row-8 shipment-row">
      <div className="shipment-1">
        <div className="image-2911"></div>
        <div className="texts-3">
          <div className="shipment-no">Shipment No 2</div>
          <span className="air-freight">Air Freight</span>
        </div>
      </div>
      <div className="texts-4">
        <div className="location">Cameroon, Douala</div>
        <span className="date">12/03/2024</span>
      </div>
      <div className="texts-5">
        <div className="location">Nigeria, Abuja</div>
        <span className="date">18/03/2024</span>
      </div>
    </div>

    {/* Expédition 3 */}
    <div className="row-9 shipment-row">
      <div className="shipment-2">
        <div className="image-2912"></div>
        <div className="texts-6">
          <div className="shipment-no">Shipment No 3</div>
          <span className="air-freight">Air Freight</span>
        </div>
      </div>
      <div className="texts-7">
        <div className="location">Cameroon, Douala</div>
        <span className="date">12/03/2024</span>
      </div>
      <div className="texts-8">
        <div className="location">Nigeria, Abuja</div>
        <span className="date">18/03/2024</span>
      </div>
    </div>
  </div>

              <div className="scroll">
              </div>
            </div>
          </div>
        </div>
        <div className="footer">
          <span className="copyright-3">
          Terms 
          </span>
          <span className="container-29">
          |
          </span>
          <span className="copyright-4">
          Privacy
          </span>
          <span className="container-30">
          |
          </span>
          <span className="copyright-5">
          Status
          </span>
          <span className="container-31">
          |
          </span>
          <span className="copyright-6">
          Data
          </span>
        </div>
      </div>
    </div>
  )
}