import './TopBarCollapseCopy.css'
export default function TopBarCollapseCopy() {
  return (
    <div className="top-bar-collapse-copy">
      <div className="container-1">
        <div className="collapse-button">
          <div className="vector-1">
          </div>
          <div className="vector-2">
          </div>
          <div className="vector-3">
          </div>
        </div>
        <div className="search-bar">
          <div className="magnifier">
          <img class="xmlid-223" src="../assets/vectors/xmlid_2235_x2.svg" />
          </div>
          <span className="search-anythingfiles-documents-task">
          Search anything,files, documents, task 
          </span>
        </div>
      </div>
      <div className="frame-92">
        <div className="container-7">
          <div className="container-2">
            <div className="mingcutetime-line">
            <img class="vector-112" src="../assets/vectors/vector_226_x2.svg" />
            </div>
            <div className="container">
            2 : 35
            </div>
          </div>
          <div className="news">
          News
          </div>
        </div>
        <span className="thurs-1-jan">
        Thurs, 1 Jan
        </span>
      </div>
      <div className="container">
      <img class="additional-menu" src="../assets/vectors/additional_menu_8_x2.svg" />
        <div className="menu">
          <div className="container-4">
            <div className="notifications">
              <div className="container-3">
                <img className="container-5" src="assets/vectors/Unknown" />
              </div>
            </div>
            <div className="languange">
              <div className="text">
              <img class="usa" src="../assets/vectors/usa_x2.svg" />
                <span className="eng">
                Eng
                </span>
              </div>
              <div className="chevron-down">
                <img className="vector-16" src="assets/vectors/Unknown" />
              </div>
            </div>
          </div>
          <div className="user-profile">
            <div className="container-6">
              <span className="nromaric">
              N. Romaric
              </span>
              <div className="group-21861">
                <img className="shape" src="assets/vectors/Unknown" />
              </div>
            </div>
            <span className="sadmin-dg">
            S Admin: DG
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}