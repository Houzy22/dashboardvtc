import React from 'react';
import ReactDOM from 'react-dom/client';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import SuperAdminDashboard from './pages/SuperAdminDashboard';
// import TopBarCollapseCopy from './pages/TopBarCollapseCopy';
const router = createBrowserRouter([
  { path: '/', element: <SuperAdminDashboard/> },
  // {
  //   path: '/',element
  // }
]);

export default function App() {
  return (
    <RouterProvider router={router} />
  );
}